More Boxes is a Drupal module which adds various type of boxes (http://drupal.org/project/boxes) to use.

So, whats in the box:
Banner Box: 
    Let you add an image or a flash file to the box using IMCE file browser.

Links Box: 
    Creates a list of links box.

Teasers List Box 1: 
    Creates a list of nodes teaser, 
    when you can select to show the list in teasers display or in a table.
    Selecting if to use pager or not and per page results. 
    Selecting filter by node type and/or by taxonomy term.
    Adding an expose filter by vocabulary and/or an exposed filter by free text search on titles. 
    Sorting by created date or by titles - ascending or descending.

Teasers List Box 2: 
    Creates a list of teasers, filter by taxonomy vocabulary, group by term.

Image Slideshow Box: 
    Creates a slideshow from a multiple image field by node ID.

To add one of these various boxes you can go to /admin/structure/block or use the Contexture (http://drupal.org/project/out_of_context) module and add those blocks to the palette.

dependencies:
Boxes (http://drupal.org/project/boxes)
Views (http://drupal.org/project/views)
Imce (http://drupal.org/project/imce)

This code initially written by Linnovate’s gavri (http://drupal.org/user/76616) for the National Roads Company of Israel site (http://www.iroads.co.il/).